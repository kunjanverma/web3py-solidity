//SPDX-License-Identifier: MIT
pragma solidity >=0.8.0;

//pragma experimental ABIEncoderV2;

//Example for :
// 1) storing struct in mapping
// 2) using a pure function
// 3) using internal function

contract Example1 {
    uint32 samplenumber = 98;

    struct Student {
        string name;
        string Address;
        uint32 id;
    }
    //student id to student struct
    mapping(uint32 => Student) studentList;

    function store(uint32 _arg1) public {
        samplenumber = _arg1;
    }

    function get(uint8 _mul) public view returns (uint32) {
        return calculateNTimes(_mul, samplenumber);
    }

    function calculateNTimes(uint8 _val1, uint32 _val2)
        internal
        pure
        returns (uint32)
    {
        return _val1 * _val2;
    }

    function addToStudentList(uint32 _id, Student memory _student) public {
        studentList[_id] = _student;
    }

    function getStudentName(uint32 _id)
        public
        view
        returns (
            string memory name,
            string memory Address,
            uint32 id
        )
    {
        return (
            studentList[_id].name,
            studentList[_id].Address,
            studentList[_id].id
        );
    }
}
