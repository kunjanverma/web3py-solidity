from solcx import compile_standard, install_solc
import json
from web3 import Web3
import os
from dotenv import load_dotenv

load_dotenv()

with open("./SimpleExample.sol", "r") as file:
    simpleExampleFile = file.read()


# Compile section

install_solc("0.8.14")
compile_code = compile_standard(
    {
        "language": "Solidity",
        "sources": {"SimpleExample.sol": {"content": simpleExampleFile}},
        "settings": {
            "outputSelection": {
                "*": {"*": ["abi", "metadata", "evm.bytecode", "evm.sourceMap"]}
            }
        },
    },
    solc_version="0.8.14",
)
with open("compiled_code.json", "w") as file:
    json.dump(compile_code, file)

# get bytecode

bytecode = compile_code["contracts"]["SimpleExample.sol"]["Example1"]["evm"][
    "bytecode"
]["object"]

# get abi

abi = compile_code["contracts"]["SimpleExample.sol"]["Example1"]["abi"]

# connect to ganache
w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:7545"))
chain_id = 1337
deploy_address = "0xa864d2eEC55e1a90e5B7F4F3918E2542fc9f4741"


# create a contract
SimpleExampleContract = w3.eth.contract(abi=abi, bytecode=bytecode)

# Get latest transaction count
nonce = w3.eth.getTransactionCount(deploy_address)

# build a transaction
transaction = SimpleExampleContract.constructor().buildTransaction(
    {
        "chainId": chain_id,
        "from": deploy_address,
        "nonce": nonce,
        "gasPrice": w3.eth.gas_price,
    }
)

# sign the transaction
deploy_private_key = os.getenv("PRIVATE_KEY")
signed_transaction = w3.eth.account.sign_transaction(transaction, deploy_private_key)

# send the signed transaction
tx_hash = w3.eth.send_raw_transaction(signed_transaction.rawTransaction)
tx_receipt = w3.eth.wait_for_transaction_receipt(tx_hash)
